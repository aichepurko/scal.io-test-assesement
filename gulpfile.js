const gulp = require('gulp');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const merge = require('merge-stream');
const autoprefixer = require('gulp-autoprefixer');

let browserSync = require('browser-sync').create();

let pathBuild = './.build/';
let pathSrc = './src/';

let pathFonts = [
  pathSrc + 'fonts/**/*'
];

gulp.task('clean', function() {
  return gulp.src(pathBuild)
  .pipe(clean({force: true}))
})

gulp.task('sass', function () {
  return gulp.src(pathSrc + '**/*.+(sass|scss)')
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(pathSrc));
});

gulp.task('pug', function () {
  gulp.src('src/pages/*.+(jade|pug)')
    .pipe(pug({pretty: '\t'}))
    .pipe(gulp.dest('.build'))
});

gulp.task('fontsDev', () => {
  return gulp.src(pathFonts)
    .pipe(gulp.dest(pathBuild + 'fonts'));
});


gulp.task('browserSync', () => {
  browserSync.init({
    server: pathBuild
  });
});

gulp.task('watch', function () {
  gulp.watch('src/**/*.+(sass|scss)', ['sass']);
  gulp.watch('src/**/*.+(css)', ['pug']).on('change', browserSync.reload);
  gulp.watch('src/**/*.+(jade|pug)', ['pug']).on('change', browserSync.reload);
});

gulp.task('build', [
  'sass',
  'pug',
  'fontsDev',
]);

gulp.task('default', [
  'build',
  'watch',
  'browserSync',
]);
